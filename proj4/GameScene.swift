//
//  GameScene.swift
//  proj4
//
//  Created by oliver on 3/7/17.
//  Copyright © 2017 oliver. All rights reserved.
//

import SpriteKit
import GameplayKit

let playerCategory : UInt32 = 0x1 << 0;
let groundCategory : UInt32 = 0x1 << 1;
let enemyCategory : UInt32 = 0x1 << 2;
let maxSpeed : CGFloat = 100;
let maxSpeedRunning : CGFloat = 200;



class GameScene: SKScene, SKPhysicsContactDelegate {
    
    var landBackground:SKTileMapNode!
    var skyBackground:SKTileMapNode!
    var enemies:SKTileMapNode!
    var player:SKSpriteNode!
    var tileSize:CGSize!
    var joystick:SKSpriteNode!
    var base:SKShapeNode!
    var aButton:SKShapeNode!
    var bButton:SKShapeNode!
    var isRunning:BooleanLiteralType!
    var walkingFrames:[SKTexture]!
    var runningFrames:[SKTexture]!
    var jumpingFrames:[SKTexture]!
    var tryAgain:SKShapeNode?

    

    override func didMove(to view: SKView) {
        loadSceneNodes()
        
    }
    
    
    func touchDown(atPoint pos : CGPoint) {
 

    }
    
    func touchMoved(toPoint pos : CGPoint) {
  
    }
    
    func touchUp(atPoint pos : CGPoint) {
        
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
   
        for t in touches{
            let location = t.location(in: self.camera!)
            
            if (self.bButton.frame.contains(location)){
                self.isRunning = true
            }
            else if(aButton.frame.contains(location) && player.physicsBody?.velocity.dy == 0){
                
                self.player.run(SKAction.animate(with: jumpingFrames, timePerFrame: 0.8, resize: false, restore: true))
                self.player.physicsBody?.applyImpulse(CGVector(dx: 0, dy: 150.0))
                
                
            }
            if let someNodeExist = self.childNode(withName: "self.tryAgain") {
                if(someNodeExist.contains(location)){
                    loadSceneNodes()
                }
            }


        }
 
    }
    
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        for t in touches {
            let location = t.location(in: self.camera!)
            if(location.x < 0 && location.y < 0){
            
                if(base.frame.contains(location)){
                    self.joystick.position = location
                }
                else{
                
                    let v = CGVector(dx: location.x - self.joystick.position.x, dy: location.y - self.joystick.position.y)
            
                    let angle = atan2(v.dy, v.dx) - CGFloat(GLKMathDegreesToRadians(90.0))
                    var degrees = -1.0 * GLKMathRadiansToDegrees(Float(angle))
                    
                    if(degrees < 0) {degrees += 360.0}
                    
                    let length:CGFloat = self.joystick.frame.size.height / 2
                    var direction:CGFloat = 1.0
                    
                    
                    if(degrees < 225 && degrees > 135){
                        print("duck")
                    }
                    else{
                        if(degrees >= 225){ direction = -1.0 }
                        walk(forward: direction)
                    }
 
                    self.player.xScale = fabs(self.player.xScale) * CGFloat(direction)
                    
            
                    let newX = sin(angle) * length
                    let newY = cos(angle) * length
            
                    self.joystick.position = CGPoint(x: self.base.position.x - newX, y: self.base.position.y + newY)
                }
            }
            
            
        }
        
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        for t in touches {
            let location = t.location(in: self.camera!)
            

            if(bButton.frame.contains(location)){
                self.isRunning = false
            }
            
        }
        self.joystick.position = self.base.position
    }
    override func touchesCancelled(_ touches: Set<UITouch>, with event: UIEvent?) {
        for t in touches { self.touchUp(atPoint: t.location(in: self)) }
    }
    
    
    func didBegin(_ contact: SKPhysicsContact){
        
    }
    
    

    func walk(forward: CGFloat){
        print(forward)
        
        
        self.player.run(SKAction.repeatForever(SKAction.animate(with: walkingFrames, timePerFrame: 0.25, resize: false, restore: true)))

        if(isRunning == true && fabs((self.player.physicsBody?.velocity.dx)!) < maxSpeedRunning){
            
                self.player.physicsBody?.applyImpulse(CGVector(dx: forward * 130.0, dy: 0.0))
        }
        else if(fabs((self.player.physicsBody?.velocity.dx)!) < maxSpeed){
                
                self.player.physicsBody?.applyImpulse(CGVector(dx: forward * 100.0, dy: 0.0))
        }
        
        
    }
    
  
    override func update(_ currentTime: TimeInterval) {
        
        //TODO: add condition for end of level
        
        if((self.player.position.x > (self.view?.bounds.width)! / 4)){
            let action = SKAction.move(to: self.player.position, duration: 0.25)
            self.camera!.run(action)
        }
        
        if(self.player.position.y < -300.0){ gameover() }
        
        if(self.player.physicsBody!.velocity.dx == 0.0 && self.player.physicsBody!.velocity.dy == 0.0) {
            self.player.removeAllActions()
            
            self.player.texture = SKTexture(imageNamed: "marioStanding.png")
        }
        print(self.player.physicsBody!.velocity.dy)
    }

    func gameover(){
        let fadeOut = SKAction.fadeOut(withDuration: 0.5)
        self.landBackground.run(fadeOut)
        self.camera!.run(fadeOut)
        
        let tryAgain = SKShapeNode(rect: CGRect(x: -150, y: -50, width: 300, height: 100), cornerRadius: 10)
        self.tryAgain = tryAgain
        
        self.tryAgain?.fillColor = SKColor.green
        
        
        let tryAgainLabel = SKLabelNode(text: "Try Again?")
        tryAgainLabel.fontColor = SKColor.black
        tryAgainLabel.fontName = "AppleSDGothicNeo-Bold"
        tryAgainLabel.verticalAlignmentMode = SKLabelVerticalAlignmentMode.center
        tryAgain.position = CGPoint(x: 0, y: 0)
        
        self.addChild(tryAgain)
        self.addChild(tryAgainLabel)
       
        
        
        
    }
    
    func loadSceneNodes(){
        
        
        physicsWorld.gravity = CGVector(dx: 0, dy: -3)
        physicsWorld.contactDelegate = self
        self.view?.showsPhysics = true
        
        var cam:SKCameraNode!
        cam = SKCameraNode()

        cam.position = self.position
        
        
        self.anchorPoint = CGPoint(x: 0.5, y: 0.5)
        
        self.physicsWorld.gravity = CGVector(dx: 0, dy: -2)

        let player = SKSpriteNode()
        
            
        self.player = player
        
        self.player.texture = SKTexture(imageNamed: "marioStanding.png")
        self.player.size = CGSize(width: 70, height: 135)
        self.player.physicsBody = SKPhysicsBody(rectangleOf: self.player.size)
        self.player.position = CGPoint(x: -500.0,y: 300.0)
        self.player.physicsBody?.categoryBitMask = playerCategory
        self.player.physicsBody?.collisionBitMask = groundCategory|playerCategory
        
        self.player.physicsBody?.allowsRotation = false
        self.player.physicsBody?.restitution = 0
        
        
      //  self.player.physicsBody!.contactTestBitMask = groundCategory
        self.player.physicsBody?.isDynamic = true
        
        addChild(self.player)

        var walkFrames = [SKTexture]()
        
        walkFrames.append(SKTexture(imageNamed: "marioStanding.png"))
        walkFrames.append(SKTexture(imageNamed: "marioWalking.png"))
        
        self.walkingFrames = walkFrames
        
        
        var jumpFrames = [SKTexture]()
        jumpFrames.append(SKTexture(imageNamed: "marioJumping.png"))
        jumpFrames.append(SKTexture(imageNamed: "marioWalking.png"))
 
        
        self.jumpingFrames = jumpFrames
        
        
        
        
        
        guard let landBackground = childNode(withName: "ground")
            as? SKTileMapNode else {
                fatalError("Background node not loaded")
        }
        self.landBackground = landBackground
        self.tileSize = landBackground.tileSize
   
        for x in 0...(landBackground.numberOfColumns-1){
            for y in 0...(landBackground.numberOfRows-1){
        
                let tile = landBackground.tileDefinition(atColumn: x, row: y)
                if(tile != nil){
                    let rect = CGRect(x: 0, y: 0, width: tileSize.width, height: tileSize.height)
                    let tileNode = SKShapeNode(rect: rect)
                    tileNode.strokeColor = SKColor(white: 0.0, alpha: 0.0)
                    
                    
                    
                    tileNode.position = self.landBackground.centerOfTile(atColumn: x, row: y)
                    
                    
                    tileNode.physicsBody = SKPhysicsBody(rectangleOf: CGSize(width: 16,height: 16), center: .zero)
                    tileNode.physicsBody?.isDynamic = false
                    tileNode.physicsBody?.collisionBitMask = playerCategory
                    
                    //tileNode.physicsBody?.contactTestBitMask = playerCategory
                    tileNode.physicsBody?.categoryBitMask = groundCategory
                    landBackground.addChild(tileNode)
                }
            }
        }
        
        
        guard let skyBackground = childNode(withName: "sky")
            as? SKTileMapNode else {
                fatalError("Sky node not loaded")
        }
        self.skyBackground = skyBackground
        /*
        guard let enemies = childNode(withName: "enemies")
            as? SKTileMapNode else {
                fatalError("Enemy node not loaded")
        }
        self.enemies = enemies

         */
        
        
        // let base = SKSpriteNode(imageNamed: "base.png")
        //  self.base = base
        

        let joystick = SKSpriteNode()
        self.joystick = joystick
        
        self.joystick.texture = SKTexture(imageNamed: "joystick.png")
        self.joystick.size = CGSize(width: 100, height: 100)
        self.joystick.position = CGPoint(x: -500, y: -240)
        self.joystick.zPosition = 1
        self.joystick.alpha = 0.5
    
        cam.addChild(self.joystick)

        
        let base = SKShapeNode()
        
        
        self.base = base
        self.base = SKShapeNode(circleOfRadius: self.joystick.frame.size.height/1.5)
        self.base.fillColor = SKColor.black
        self.base.strokeColor = SKColor.red
        self.base.lineWidth = 10
        self.base.alpha = 0.6
        
        self.base.position = self.joystick.position
        
        cam.addChild(self.base)
        
        let aButton =  SKShapeNode(circleOfRadius: 62.5)
        self.aButton = aButton
        self.aButton.position = CGPoint(x: 380, y: -240)
        self.aButton.fillColor = SKColor.red
        self.aButton.strokeColor = SKColor.black
        
        let aLabel = SKLabelNode(text: "A")
        aLabel.fontColor = SKColor.black
        aLabel.fontName = "AppleSDGothicNeo-Bold"
        aLabel.verticalAlignmentMode = SKLabelVerticalAlignmentMode.center
        aLabel.position = self.aButton.position
        self.aButton.alpha = 0.8

        
        
        
        cam.addChild(aLabel)
        cam.addChild(self.aButton)
        
        let bButton =  SKShapeNode(circleOfRadius: 62.5)
        self.bButton = bButton
        self.bButton.position = CGPoint(x: 525, y: -240)
        self.bButton.fillColor = SKColor.red
        self.bButton.strokeColor = SKColor.black
        
        self.bButton.alpha = 0.8
        
        

        let bLabel = SKLabelNode(text: "B")
        bLabel.fontColor = SKColor.black
        bLabel.fontName = "AppleSDGothicNeo-Bold"
        bLabel.verticalAlignmentMode = SKLabelVerticalAlignmentMode.center
        bLabel.position = self.bButton.position
    
        
        cam.addChild(self.bButton)
        cam.addChild(bLabel)

        self.isRunning = false
        
        self.camera = cam
        
        self.addChild(cam)

        
        
    }
}


